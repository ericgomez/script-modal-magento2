/**
 * Change Version: 2.0.22
 * */

const d = document,
  w = window,
  $modalLogin = d.createElement('div'),
  $modalContainer = d.createElement('div'),
  $modalCClose = d.createElement('div'),
  $modalClose = d.createElement('span'),
  $modalCForm = d.createElement('div'),
  $modalTitle = d.createElement('h2'),
  $modalInput = d.createElement('input'),
  $modalErrorMessage = d.createElement('p'),
  $modalButton = d.createElement('button'),
  // ************* ****************
  $bannerMenu = d.createElement('div'),
  $bannerLogin = d.createElement('p'),
  $bannerLoginButton = d.createElement('button'),
  // ************* ****************
  $modalExit = d.createElement('div'),
  $modalExitContainer = d.createElement('div'),
  $modalExitCClose = d.createElement('div'),
  $modalExitClose = d.createElement('span'),
  $modalExitForm = d.createElement('div'),
  $modalExitTitle = d.createElement('h5'),
  $modalExitSubtitle = d.createElement('p'),
  $modalExitButtons = d.createElement('div'),
  $modalExitButton = d.createElement('button'),
  $modalExitButtonCancel = d.createElement('button');

// *********** location *********
const isViu = window.location.href.includes("viu"),
  serverUrl = isViu ? "https://viustage.mavi.mx/api/ext/mavi" : 'https://mueblesstage.mavi.mx/api/ext/mavi',
  serverUrlMagento = isViu ? 'https://mcstage.viu.mx/rest/V1/mavi-registerseller' : 'https://mcstage.mueblesamerica.mx/rest/V1/mavi-registerseller';

// ********** localStorage *********
const dataPromoter = localStorage.getItem('promoter'),
  cartId = localStorage.getItem('cartId') || '',
  checkoutId = localStorage.getItem('checkout-session');

const loadModal = (e) => {
  $modalLogin.classList.add('login-float');
  $modalLogin.setAttribute('id', 'myModal');
  $modalClose.classList.add('close');
  $modalClose.innerHTML = '&#215;';
  $modalTitle.textContent = 'Num. Empleado:';
  $modalInput.placeholder = 'E000001';
  $modalInput.title = 'Num. Empleado no es valido';
  $modalInput.pattern = '^([a-zA-Z]{1})[0-9]{6,6}$';
  $modalInput.setAttribute('id', 'login-input-modal');;
  $modalInput.required = true;
  $modalErrorMessage.textContent = 'Num. Empleado:';
  $modalButton.textContent = 'Login';
  $modalButton.disabled = true;
  $modalButton.setAttribute('id', 'myBtn');

  $modalCClose.appendChild($modalClose);
  $modalContainer.appendChild($modalCClose);
  $modalCForm.appendChild($modalTitle);
  $modalCForm.appendChild($modalInput);
  $modalContainer.appendChild($modalCForm);
  $modalContainer.appendChild($modalErrorMessage);
  $modalContainer.appendChild($modalButton);
  $modalLogin.appendChild($modalContainer);
  d.body.appendChild($modalLogin);

  $bannerMenu.setAttribute(
    'style',
    `display: flex;
    justify-content: space-between;
    background-color: #f3b516;
    position: sticky;
    top: 0;
    z-index: 99;`
  );

  $modalErrorMessage.setAttribute(
    'style',
    `display: none;
    justify-content: center;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 20px;
    margin-top: 0;
    color: red;`
  );

  $bannerLogin.setAttribute(
    'style',
    `margin: 0;
    border: none;
    font-weight: bold;
    font-size: 10px;
    padding: 2px;`
  );

  $bannerLoginButton.setAttribute(
    'style',
    `background-color: transparent;
    border: none;
    font-weight: bold;
    text-decoration: underline;
    font-size: 10px;
    padding: 0 10px;
    cursor: pointer;`
  );

  $modalCClose.setAttribute(
    'style',
    `justify-content: end;
    display: flex;`
  );

  $modalClose.setAttribute(
    'style',
    `font-size: 30px;
    cursor: pointer;`
  );

  $modalCForm.setAttribute(
    'style',
    `display: flex;
    justify-content: center;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 20px;
    margin-top: 0;`
  );

  if (w.innerWidth <= 768) {
    $modalCForm.setAttribute(
      'style',
      `display: block;
      justify-content: center;
      margin: 40px;
      margin-top: 0;`
    );
  }

  $modalTitle.setAttribute(
    'style',
    `margin-left: 20px;
    margin-right: 20px;
    font-size: 16px;
    font-weight: bold;
    font-family: 'Montserrat', Verdana, Geneva, Tahoma, sans-serif;`
  );

  $modalInput.setAttribute(
    'style',
    `background: #f9f8f8;
    color: #051c2c;
    border-radius: 15px;
    letter-spacing: -0.5px;
    font-size: 16px !important;
    height: 40px;
    padding-left: 20px;
    padding-right: 20px;
    margin: auto;
    border: none;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    font-family: 'Montserrat', Verdana, Geneva, Tahoma, sans-serif;`
  );

  $modalButton.setAttribute(
    'style',
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #051c2c;
    margin: 5px auto;
    overflow: hidden;`
  );

  $modalLogin.setAttribute(
    'style',
    `display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 100;`
  );

  $modalContainer.setAttribute(
    'style',
    `background-color: #fff;
    position: absolute;
    width: 50%;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    margin: 0 auto;
    max-width: calc(100% - 30px);
    max-height: 100%;
    padding-top: 20px;
    padding-bottom: 20px;
    padding-left: 15px;
    padding-right: 15px;
    min-width: 330px;`
  );

  // check session state on mounted page
  if (cartId) {
    fetch(`${serverUrl}/check-session`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "cartId": `${cartId}`,
        "isViu": isViu
      })
    })
      .then((res) => (res.ok ? res.json() : Promise.reject(res)))
      .then((json) => {
        if (json.result.length !== 0) {
          const { name, payroll } = JSON.parse(dataPromoter);
          $bannerLogin.textContent = `Bienvenido ${name}, ${payroll}`;
          $bannerLoginButton.classList.add('btn-exited');
          $bannerLoginButton.textContent = 'Salir';
          $bannerMenu.appendChild($bannerLogin);
          $bannerMenu.appendChild($bannerLoginButton);

          document.body.insertBefore($bannerMenu, document.body.firstChild);

          // const $header = document.querySelector('header');
          // $header.style.marginTop = '15px';

          setTimeout(
            () => {
              const $header = document.querySelector('header');
              $header.style.marginTop = '15px';

              const $btnExited = document.querySelector('.btn-exited');
              $btnExited.onclick = function () {
                generateModalExit();
                $modalExit.style.display = 'block';
              };
            },
            2000
          );
        } else {
          if (dataPromoter)
            localStorage.removeItem('promoter');

          if ($bannerMenu)
            $bannerMenu?.parentNode?.removeChild($bannerMenu);

          if (window.location.pathname === '/promotor' && !dataPromoter)
            $modalLogin.style.display = 'block';
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }
  else if (dataPromoter && !checkoutId) {
    const { name, payroll } = JSON.parse(dataPromoter);
    $bannerLogin.textContent = `Bienvenido ${name}, ${payroll}`;
    $bannerLoginButton.classList.add('btn-exited');
    $bannerLoginButton.textContent = 'Salir';
    $bannerMenu.appendChild($bannerLogin);
    $bannerMenu.appendChild($bannerLoginButton);

    document.body.insertBefore($bannerMenu, document.body.firstChild);

    // const $header = document.querySelector('header');
    // $header.style.marginTop = '15px';

    setTimeout(
      () => {
        const $header = document.querySelector('header');
        $header.style.marginTop = '15px';

        const $btnExited = document.querySelector('.btn-exited');
        $btnExited.onclick = function () {
          generateModalExit();
          $modalExit.style.display = 'block';
        };
      },
      2000
    );
  }
  else if (window.location.pathname === '/promotor') {
    $modalLogin.style.display = 'block';
  }

  d.addEventListener('keyup', (e) => {
    if (e.target.matches(`[id='login-input-modal']`)) {
      e.target.value = e.target.value.toUpperCase();
      const $input = e.target,
        pattern = $input.pattern || $input.dataset.pattern;

      if ($input.value === '') {
        $modalErrorMessage.textContent = 'El numero de empleado es requerido.';
        return $modalErrorMessage.setAttribute(
          'style',
          `display: flex;
          justify-content: center;
          margin-left: 40px;
          margin-right: 40px;
          margin-bottom: 20px;
          margin-top: 0;
          color: red;`
        );
      }

      if (pattern && $input.value !== '') {
        const regex = new RegExp(pattern);
        $modalErrorMessage.textContent = 'El numero de empleado es no es valido.';
        if (!regex.exec($input.value)) {
          $modalErrorMessage.setAttribute(
            'style',
            `display: flex;
            justify-content: center;
            margin-left: 40px;
            margin-right: 40px;
            margin-bottom: 20px;
            margin-top: 0;
            color: red;`
          );

          $modalButton.disabled = true;
        } else {
          $modalErrorMessage.setAttribute(
            'style',
            `display: none;
            justify-content: center;
            margin-left: 40px;
            margin-right: 40px;
            margin-bottom: 20px;
            margin-top: 0;
            color: red;`
          );

          $modalButton.disabled = false;
        }
      }
    }
  });

  // login employee when click in button native add product in the cart
  d.addEventListener('click', (e) => {
    setTimeout(() => {
      if (e.target.matches('.button-content') || e.target.matches('.button-buy-now')) {
        if (dataPromoter) {
          const { payroll } = JSON.parse(dataPromoter);

          fetch(`${serverUrl}/login-employee`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "cartId": `${cartId}`,
              "payroll": `${payroll}`,
              "isViu": isViu
            })
          })
            .then((res) => (res.ok ? res.json() : Promise.reject(res)))
            .then((json) => {
              console.log(json);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      }
    }, 2000);
  });

  mouseover(
    $modalButton,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #009bdb;
    margin: 5px auto;
    overflow: hidden;`
  );

  mouseout(
    $modalButton,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #051c2c;
    margin: 5px auto;
    overflow: hidden;`
  );

  // check session state in the checkout
  setTimeout(() => {
    if (checkoutId) {
      const cartId = checkoutId;
      fetch(`${serverUrlMagento}/checksession`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "cartId": `${cartId}`
        })
      })
        .then((res) => (res.ok ? res.json() : Promise.reject(res)))
        .then((data) => {
          if (data.length !== 0) {
            fetch(`${serverUrlMagento}/getemployee`, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "Nomina": `${data}`
              })
            })
              .then((res) => (res.ok ? res.json() : Promise.reject(res)))
              .then((user) => {
                if (user.length !== 0) {
                  $modalLogin.style.display = 'none';

                  $bannerLogin.textContent = `Bienvenido ${user.name}, ${user.payroll}`;
                  $bannerLoginButton.classList.add('btn-exited');
                  $bannerLoginButton.textContent = 'Salir';
                  $bannerMenu.appendChild($bannerLogin);
                  $bannerMenu.appendChild($bannerLoginButton);

                  document.body.insertBefore($bannerMenu, document.body.firstChild);

                  // localStorage.setItem('promoter', JSON.stringify({
                  //   "name": `${user.result.name}`,
                  //   "payroll": `${user.result.payroll}`
                  // }));
                } else {
                  if ($bannerMenu)
                    $bannerMenu.parentNode.removeChild($bannerMenu);
                }
              });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, 2000);
};

// hide modal
$modalClose.onclick = function () {
  $modalLogin.style.display = 'none';
};

// check and employee exists in database
$modalButton.onclick = function () {
  fetch(`${serverUrl}/get-employee`, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "Nomina": `${$modalInput.value}`,
      "isViu": isViu
    })
  })
    .then((res) => (res.ok ? res.json() : Promise.reject(res)))
    .then((json) => {
      if (json.result.length !== 0) {
        $modalLogin.style.display = 'none';
        $bannerLogin.textContent = `Bienvenido ${json.result.name}, ${json.result.payroll}`;
        $bannerLoginButton.classList.add('btn-exited');
        $bannerLoginButton.textContent = 'Salir';
        $bannerMenu.appendChild($bannerLogin);
        $bannerMenu.appendChild($bannerLoginButton);

        document.body.insertBefore($bannerMenu, document.body.firstChild);

        const $header = document.querySelector('header');
        $header.style.marginTop = '15px';

        localStorage.setItem('promoter', JSON.stringify({
          "name": `${json.result.name}`,
          "payroll": `${json.result.payroll}`,
          "isViu": isViu
        }));

        // if exists cartId 
        if (cartId) {
          fetch(`${serverUrl}/login-employee`, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "cartId": `${cartId}`,
              "payroll": `${json.result.payroll}`,
              "isViu": isViu
            })
          })
            .then((res) => (res.ok ? res.json() : Promise.reject(res)))
            .then((json) => {
              console.log(json);
            })
            .catch((err) => {
              console.log(err);
            });
        }
      } else {
        $modalErrorMessage.textContent = 'El numero de empledo no fue localizado o no existe.';
        $modalErrorMessage.setAttribute(
          'style',
          `display: flex;
          justify-content: center;
          margin-left: 40px;
          margin-right: 40px;
          margin-bottom: 20px;
          margin-top: 0;
          color: red;`
        );
      }
    })
    .catch((err) => {
      console.log(err);
    });
};


$bannerLoginButton.onclick = function () {
  $modalLogin.style.display = 'none';
  generateModalExit();
  $modalExit.style.display = 'block';
};

$modalExitClose.onclick = function () {
  $modalExit.style.display = 'none';
};

// logout employee
$modalExitButton.onclick = function () {
  if (cartId) {
    logout(`${serverUrl}/logout-employee`, cartId);
  }
  else if (checkoutId) {
    logout(`${serverUrlMagento}/logoutemployee`, checkoutId);
  }

  const $nav = document.querySelector('nav');
  if ($nav)
    $nav.style.marginTop = '0';

  const $header = document.querySelector('header');
  if ($header)
    $header.style.marginTop = '0';

  localStorage.removeItem('promoter');
  $modalExit.style.display = 'none';

  setTimeout(
    () => {
      if ($bannerMenu)
        $bannerMenu.parentNode.removeChild($bannerMenu);

      const $header = document.querySelector('.pop-container');
      if ($header)
        $header.parentNode.removeChild($header);
    },
    500
  );
};

const logout = (url, cartId) => {
  fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      "cartId": cartId,
      "isViu": isViu
    })
  })
    .then((res) => (res.ok ? res.json() : Promise.reject(res)))
    .then((json) => {
      console.log(json);
    })
    .catch((err) => {
      console.log(err);
    });
};

$modalExitButtonCancel.onclick = function () {
  $modalExit.style.display = 'none';
};

const generateModalExit = () => {
  $modalExit.setAttribute(
    'style',
    `position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 100;`
  );

  $modalExitContainer.setAttribute(
    'style',
    `background-color: #fff;
    position: absolute;
    width: 50%;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    margin: 0 auto;
    max-width: calc(100% - 30px);
    max-height: 100%;
    padding-top: 20px;
    padding-bottom: 20px;
    padding-left: 15px;
    padding-right: 15px;
    min-width: 330px;`
  );

  $modalExitCClose.setAttribute(
    'style',
    `justify-content: end;
    display: flex;`
  );

  $modalExitClose.setAttribute(
    'style',
    `font-size: 30px;
    cursor: pointer;`
  );

  $modalExitForm.setAttribute(
    'style',
    `text-align: center;
    margin-bottom: 40px;
    margin-top: 0;
    margin-left: 0;
    margin-right: 0;`
  );

  $modalExitTitle.setAttribute(
    'style',
    `font-size: 16px;
    width: 100%;
    font-family: 'Montserrat', Verdana, Geneva, Tahoma, sans-serif;
    margin-top: 0;
    margin-bottom: 20px;`
  );

  $modalExitSubtitle.setAttribute(
    'style',
    `font-size: 14px;
    width: 100%;
    font-family: 'Montserrat', Verdana, Geneva, Tahoma, sans-serif;
    margin-top: 0;
    margin-bottom: 20px;`
  );

  $modalExitButtons.setAttribute(
    'style',
    `display: flex;
    justify-content: center;
    margin: 40px;`
  );

  $modalExitButton.setAttribute(
    'style',
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #051c2c;
    margin: 5px auto;
    overflow: hidden;`
  );

  $modalExitButtonCancel.setAttribute(
    'style',
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #8b8b8b;
    margin: 5px auto;
    overflow: hidden;`
  );

  mouseover(
    $modalExitButton,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #009bdb;
    margin: 5px auto;
    overflow: hidden;`
  );

  mouseout(
    $modalExitButton,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #051c2c;
    margin: 5px auto;
    overflow: hidden;`
  );

  mouseover(
    $modalExitButtonCancel,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #009bdb;
    margin: 5px auto;
    overflow: hidden;`
  );

  mouseout(
    $modalExitButtonCancel,
    `text-decoration: none;
    color: #fff;
    font-size: 15px;
    font-weight: 700;
    line-height: 20px;
    cursor: pointer;
    position: relative;
    display: flex;
    -webkit-box-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    align-items: center;
    -webkit-appearance: none;
    padding: 10px 50px;
    border-radius: 10px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
    background-color: #8b8b8b;
    margin: 5px auto;
    overflow: hidden;`
  );

  $modalExit.classList.add('exit-float');
  $modalExit.setAttribute('id', 'myModal');
  $modalExitClose.classList.add('close');
  $modalExitClose.innerHTML = '&#215;';
  $modalExitTitle.textContent = '¿Estás seguro de cerrar sesión?';
  $modalExitSubtitle.textContent =
    'Ten en cuenta que para generar tus pedidos debes mantener tu sesión activa.';
  $modalExitButton.textContent = 'Salir';
  $modalExitButton.setAttribute('id', 'myBtn');
  $modalExitButtonCancel.textContent = 'Cancelar';
  $modalExitButtonCancel.setAttribute('id', 'myBtn');

  $modalExitCClose.appendChild($modalExitClose);
  $modalExitContainer.appendChild($modalExitCClose);
  $modalExitForm.appendChild($modalExitTitle);
  $modalExitForm.appendChild($modalExitSubtitle);
  $modalExitContainer.appendChild($modalExitForm);
  $modalExitButtons.appendChild($modalExitButton);
  $modalExitButtons.appendChild($modalExitButtonCancel);
  $modalExitContainer.appendChild($modalExitButtons);
  $modalExit.appendChild($modalExitContainer);
  d.body.appendChild($modalExit);
};

// Button hover
const mouseover = ($component, styles) => {
  $component.addEventListener(
    'mouseover',
    function () {
      $component.setAttribute('style', `${styles}`);
    },
    false
  );
};

const mouseout = ($component, styles) => {
  $component.addEventListener(
    'mouseout',
    function () {
      $component.setAttribute('style', `${styles}`);
    },
    false
  );
};

// const mediaQuery320 = window.matchMedia('(min-width: 320px)');
const mediaQuery480 = window.matchMedia('(min-width: 480px)');
const mediaQueryMax480 = window.matchMedia('(max-width: 768px)');
// const mediaQuery600 = window.matchMedia('(min-width: 600px)');
const mediaQuery768 = window.matchMedia('(min-width: 768px)');
// const mediaQuery1024 = window.matchMedia('(min-width: 1024px)');

function handleMobileChange(e) {
  if (e.matches) {
  }
}

function handleTabletChange(e) {
  if (e.matches) {
    $modalCForm.setAttribute(
      'style',
      `display: block;
      justify-content: center;
      margin: 40px;
      margin-top: 0;`
    );
  }
}

function handleLandscapeChange(e) {
  if (e.matches) {
    $modalCForm.setAttribute(
      'style',
      `display: flex;
justify-content: center;
margin: 40px;
margin-top: 0;`
    );
  }
}

mediaQueryMax480.addListener(handleTabletChange);
mediaQuery480.addListener(handleTabletChange);
mediaQuery768.addListener(handleLandscapeChange);

handleTabletChange(mediaQuery480);
handleTabletChange(mediaQuery768);

// ---- SCROLL ----
w.addEventListener('scroll', (e) => {
  const { scrollTop, clientHeight, scrollHeight } = d.documentElement;
  const elemScrollTop = w.pageYOffset || scrollTop;

  const $nav = document.querySelector('nav');

  if (elemScrollTop >= 90) {
    if (dataPromoter) {
      $nav.style.marginTop = '15px';
    }
  } else {
    if (dataPromoter) {
      $nav.style.marginTop = '0';
    }
  }
});

// load modal in the DOM
(d.readyState == 'loading')
  ? d.addEventListener('DOMContentLoaded', loadModal)
  : loadModal();
